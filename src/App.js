import React from 'react';
import DogComponent from './components/DogComponent';

function App() {
  return (
    <div>
      <DogComponent/>
    </div>
  );
} 

export default App;
