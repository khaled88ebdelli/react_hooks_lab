import React from "react";
import useFetch from "./shared/useFetch";
const imgStyle = {
  display: "block",
  marginLeft: "auto",
  marginRight: "auto",
  width: "50%"
};
const DogComponent = () => {
  const { response, error } = useFetch(
    "https://dog.ceo/api/breeds/image/random"
  );
  if (!response) {
    return <div>Loading ...</div>;
  }
  if (!!error) {
    return <div>Error occurred </div>;
  }
  const dogName = response.status;
  const imageDog = response.message;

  return (
    <div>
      <div>
        <h3>{dogName}</h3>
        <div>
          <img style={imgStyle} src={imageDog} alt="avatar" />
        </div>
      </div>
    </div>
  );
};
export default DogComponent;
